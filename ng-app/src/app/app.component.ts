import { Component, OnInit } from '@angular/core';
import { QuestionDto, TagDto } from './app.dtos';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { ApiService } from './api.service';
import { FormControl } from '@angular/forms';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material';

export const DISPLAY: string = "show";
export const HIDE: string = "hide";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  loadingSpinnerClass: string;
  questionsArray: QuestionDto[] = [];
  tagsArray: TagDto[] = [];
  tagsFormControl: FormControl = new FormControl();
  filteredTagsArray: Observable<TagDto[]>;
  selectedTagsArray: TagDto[] = [];
  orderArrayValues: string[] = ['desc', 'asc'];
  order: string;
  title: string;
  fromDate: Date;
  toDate: Date;
  pageSize: number = 25;
  pageIndex: number;

  constructor(private apiService: ApiService, private snackBar: MatSnackBar) {
  }

  ngOnInit() {
    this.loadPopularTags();
  }

  loadPopularTags() {
    this.loadingSpinnerClass = DISPLAY;
    this.apiService.getPopularTags().subscribe(
      (data: TagDto[]) => {
        this.tagsArray = data;
        this.filteredTagsArray = this.tagsFormControl.valueChanges.pipe(
          startWith(''),
          map(value => value ? this.filterArrayByNameProperty(value, this.tagsArray) : this.tagsArray.slice())
        );
        this.loadingSpinnerClass = HIDE;
      },
      err => {
        this.openSnackBar('Could not load tags from server! Please contact the administrator!', 'Close');
        this.loadingSpinnerClass = HIDE;
      }
    );
  }

  searchQuestions() {
    let fromDateTimestamp = this.getTimestampInSeconds(this.fromDate);
    let toDateTimestamp = this.getTimestampInSeconds(this.toDate);
    let tags: string = '';
    this.selectedTagsArray.forEach(tag => {
      tags += tag.name;
      tags += ';';
    });

    if (tags) {
      tags = tags.slice(0, -1);
    }
    this.loadingSpinnerClass = DISPLAY;
    this.apiService.getQuestions(fromDateTimestamp, toDateTimestamp, this.order, tags, this.title, this.pageSize, this.pageIndex).subscribe(
      (data: QuestionDto[]) => {
        this.questionsArray = data;
        this.loadingSpinnerClass = HIDE;
      },
      err => {
        this.openSnackBar('Could not load questions from server! Please contact the administrator!', 'Close');
        this.loadingSpinnerClass = HIDE;
      }
    );
  }

  onOrderChange(event, order: string) {
    if (event.source.selected) {
      this.order = order;
    }
  }

  updateSelectedTagsArray(event, selectedTag: TagDto) {
    if (event.source.selected) {
      this.selectedTagsArray.push(selectedTag);
    } else {
      this.selectedTagsArray = this.selectedTagsArray.filter(value => value !== selectedTag);
    }
  }

  openLink(link: string) {
    window.open(link, '_blank');
  }

  private getTimestampInSeconds(inputDate: Date) {
    if (inputDate) {
      return Math.floor(inputDate.getTime() / 1000);
    }
  }

  private filterArrayByNameProperty(value: string, array: any[]): any[] {
    const filterValue = value.toLowerCase();
    return array.filter(option =>
      option.name.toLowerCase().includes(filterValue));
  }

  private openSnackBar(message: string, action: string) {
    const snackBarConfig = new MatSnackBarConfig();
    snackBarConfig.horizontalPosition = 'center';
    snackBarConfig.verticalPosition = 'top';
    snackBarConfig.panelClass = 'custom-snackbar';
    snackBarConfig.duration = 2000;
    this.snackBar.open(message, action, snackBarConfig);
  }

  nextPageAction(event) {
    this.pageIndex = event.pageIndex;
    this.pageSize = event.pageSize;
    this.searchQuestions();
  }

}