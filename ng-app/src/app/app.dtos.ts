export class QuestionDto {
    title: string;
    score: number;
    tags: string[];
    answer_count: number;
    view_count: number;
    link: string;
}

export class TagDto {
    name: string;
    count: number;
}
