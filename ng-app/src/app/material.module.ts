import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatFormFieldModule, MatInputModule, MatButtonModule, MatToolbarModule, MatNativeDateModule } from '@angular/material';
import { MatStepperModule } from '@angular/material/stepper';
import { MatListModule } from '@angular/material/list';
import { MatSelectModule } from '@angular/material/select';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDividerModule } from '@angular/material/divider'
import { MatCardModule } from '@angular/material/card';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTabsModule } from '@angular/material/tabs';
import { MatIconModule } from '@angular/material/icon';
import { MatSnackBarModule } from '@angular/material';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatRippleModule } from '@angular/material/core';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatChipsModule } from '@angular/material/chips';


@NgModule({
  imports: [MatChipsModule, MatProgressBarModule, MatExpansionModule, MatRippleModule, MatAutocompleteModule, MatSnackBarModule, MatIconModule, MatTabsModule, MatTableModule, MatPaginatorModule, MatCardModule, MatDividerModule, MatDatepickerModule, MatNativeDateModule, MatGridListModule, MatProgressSpinnerModule, MatSelectModule, MatListModule, MatStepperModule, MatFormFieldModule, MatInputModule, CommonModule, MatButtonModule, MatToolbarModule],
  exports: [MatChipsModule, MatProgressBarModule, MatExpansionModule, MatRippleModule, MatAutocompleteModule, MatSnackBarModule, MatIconModule, MatTabsModule, MatTableModule, MatPaginatorModule, MatCardModule, MatDividerModule, MatDatepickerModule, MatNativeDateModule, MatGridListModule, MatProgressSpinnerModule, MatSelectModule, MatListModule, MatStepperModule, MatFormFieldModule, MatInputModule, CommonModule, MatButtonModule, MatToolbarModule],
})
export class MaterialModule { }