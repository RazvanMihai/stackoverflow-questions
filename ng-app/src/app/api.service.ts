import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class ApiService {
  QUESTIONS_CONTROLLER_URL = '/stackoverflow-questions/questions';
  TAGS_CONTROLLER_URL = '/stackoverflow-questions/tags';

  constructor(private httpClient: HttpClient) { }

  getQuestions(fromdate, todate, order: string, tags: string, intitle: string, pageSize: number, pageIndex: number) {
    let params: HttpParams = new HttpParams();
    if (fromdate) {
      params = params.append('fromdate', fromdate);
    }
    if (todate) {
      params = params.append('todate', todate);
    }
    if (order) {
      params = params.append('order', order);
    }
    if (tags) {
      params = params.append('tagged', `${tags}`);
    }
    if (intitle) {
      params = params.append('intitle', intitle);
    }
    params = params.append('pagesize', pageSize.toString());
    if (pageIndex) {
      params = params.append('page', (pageIndex + 1).toString());
    }
    return this.httpClient.get(this.QUESTIONS_CONTROLLER_URL, { params: params });
  }

  getPopularTags() {
    return this.httpClient.get(this.TAGS_CONTROLLER_URL);
  }
}
