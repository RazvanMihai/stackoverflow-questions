(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error('Cannot find module "' + req + '".');
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/api.service.ts":
/*!********************************!*\
  !*** ./src/app/api.service.ts ***!
  \********************************/
/*! exports provided: ApiService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ApiService", function() { return ApiService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ApiService = /** @class */ (function () {
    function ApiService(httpClient) {
        this.httpClient = httpClient;
        this.QUESTIONS_CONTROLLER_URL = '/stackoverflow-questions/questions';
        this.TAGS_CONTROLLER_URL = '/stackoverflow-questions/tags';
    }
    ApiService.prototype.getQuestions = function (fromdate, todate, order, tags, intitle, pageSize, pageIndex) {
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]();
        if (fromdate) {
            params = params.append('fromdate', fromdate);
        }
        if (todate) {
            params = params.append('todate', todate);
        }
        if (order) {
            params = params.append('order', order);
        }
        if (tags) {
            params = params.append('tagged', "" + tags);
        }
        if (intitle) {
            params = params.append('intitle', intitle);
        }
        params = params.append('pagesize', pageSize.toString());
        if (pageIndex) {
            params = params.append('page', (pageIndex + 1).toString());
        }
        return this.httpClient.get(this.QUESTIONS_CONTROLLER_URL, { params: params });
    };
    ApiService.prototype.getPopularTags = function () {
        return this.httpClient.get(this.TAGS_CONTROLLER_URL);
    };
    ApiService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], ApiService);
    return ApiService;
}());



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".spinner {\r\n    position: absolute;\r\n    left: 50%;\r\n    top: 50%;\r\n    z-index: 12;\r\n}\r\n\r\n.overlay {\r\n    background: gray;\r\n    position: fixed;\r\n    z-index: 2;\r\n    top: 0;\r\n    right: 0;\r\n    bottom: 0;\r\n    left: 0;\r\n    opacity: 0.6;\r\n}\r\n\r\n.show {\r\n    display: block;\r\n}\r\n\r\n.hide {\r\n    display: none;\r\n}\r\n\r\n.custom-header .mat-expansion-panel-header-title, .custom-header .mat-expansion-panel-header-description {\r\n    flex-basis: 0;\r\n}\r\n\r\n.custom-headers .mat-expansion-panel-header-description {\r\n    justify-content: space-between;\r\n    align-items: center;\r\n}\r\n\r\n.mat-expansion-panel-header.mat-expanded {\r\n    background: #3f51b5;\r\n    color: #fff !important;\r\n}\r\n\r\n.mat-expansion-panel-header.mat-expanded:hover {\r\n    background: #3f51b5;\r\n    color: #fff !important;\r\n}\r\n\r\n.mat-expansion-panel-header.mat-expanded:focus {\r\n    background: #3f51b5;\r\n    color: #fff !important;\r\n}\r\n\r\n.mat-expansion-panel-header-title {\r\n    color: inherit;\r\n}\r\n\r\n.mat-expansion-panel-header-description {\r\n    color: inherit;\r\n}\r\n\r\n.custom-toolbar {\r\n    font-size: 16px;\r\n    height: 48px;\r\n}\r\n\r\n.mat-action-row {\r\n    justify-content: flex-start;\r\n}"

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"overlay\" [ngClass]=\"loadingSpinnerClass\"></div>\n<mat-spinner class=\"spinner\" [ngClass]=\"loadingSpinnerClass\" [diameter]=60></mat-spinner>\n<mat-toolbar class=\"custom-toolbar\">\n  <mat-toolbar-row>\n    <mat-toolbar-row>\n      <h1>Stackoverflow wrapper</h1>\n    </mat-toolbar-row>\n\n    <mat-toolbar-row>\n      <mat-form-field>\n        <mat-label>From date:</mat-label>\n        <input matInput [matDatepicker]=\"fromDatePicker\" [(ngModel)]=\"fromDate\">\n        <mat-datepicker-toggle matSuffix [for]=\"fromDatePicker\"></mat-datepicker-toggle>\n        <mat-datepicker #fromDatePicker></mat-datepicker>\n      </mat-form-field>\n    </mat-toolbar-row>\n\n    <mat-toolbar-row>\n      <mat-form-field>\n        <mat-label>To date:</mat-label>\n        <input matInput [matDatepicker]=\"toDatePicker\" [(ngModel)]=\"toDate\">\n        <mat-datepicker-toggle matSuffix [for]=\"toDatePicker\"></mat-datepicker-toggle>\n        <mat-datepicker #toDatePicker></mat-datepicker>\n      </mat-form-field>\n    </mat-toolbar-row>\n\n    <mat-toolbar-row>\n      <mat-form-field>\n        <mat-label>Tags: </mat-label>\n        <mat-select multiple [(value)]=\"selectedTagsString\">\n          <mat-option *ngFor=\"let tag of tagsArray\" [value]=\"tag.name\"\n            (onSelectionChange)=\"updateSelectedTagsArray($event, tag)\">{{tag.name}} </mat-option>\n        </mat-select>\n      </mat-form-field>\n    </mat-toolbar-row>\n\n    <mat-toolbar-row>\n      <mat-form-field>\n        <mat-label>Title:</mat-label>\n        <input matInput [(ngModel)]=\"title\">\n      </mat-form-field>\n    </mat-toolbar-row>\n\n    <mat-form-field>\n      <mat-label>Sort order: </mat-label>\n      <mat-select value=\"{{orderArrayValues[0]}}\">\n        <mat-option *ngFor=\"let order of orderArrayValues\" value=\"{{order}}\"\n          (onSelectionChange)=\"onOrderChange($event, order)\">\n          {{order}}\n        </mat-option>\n      </mat-select>\n    </mat-form-field>\n\n    <mat-toolbar-row>\n      <button mat-button (click)=\"searchQuestions()\">\n        <mat-icon>search</mat-icon>\n        Search\n      </button>\n    </mat-toolbar-row>\n  </mat-toolbar-row>\n\n</mat-toolbar>\n<mat-toolbar class=\"custom-toolbar\">\n  <mat-toolbar-row>\n    <mat-chip-list>\n      <mat-chip *ngFor=\"let tag of selectedTagsArray;\" color=\"primary\">{{tag.name}}</mat-chip>\n    </mat-chip-list>\n  </mat-toolbar-row>\n\n  <mat-paginator [length]=\"100\" [pageSize]=\"pageSize\" [pageSizeOptions]=\"[25, 50, 100]\"\n    (page)=\"pageEvent = nextPageAction($event)\"></mat-paginator>\n</mat-toolbar>\n\n\n\n<mat-accordion class=\" custom-header\">\n  <mat-expansion-panel hideToggle *ngFor=\"let question of questionsArray; let i = index;\">\n    <mat-expansion-panel-header>\n      <mat-panel-title>\n        Votes: {{question.score}}\n      </mat-panel-title>\n      <mat-panel-description>{{question.title}}\n      </mat-panel-description>\n    </mat-expansion-panel-header>\n\n    <mat-grid-list cols=\"4\" rowHeight=\"3:1\">\n      <mat-grid-tile>\n        <mat-list role=\"list\">\n          <mat-list-item role=\"listitem\"><b>Votes:</b> {{question.score}}</mat-list-item>\n          <mat-list-item role=\"listitem\"><b>Answers:</b> {{question.answer_count}}</mat-list-item>\n\n        </mat-list>\n      </mat-grid-tile>\n\n      <mat-grid-tile>\n        <mat-list role=\"list\">\n          <mat-list-item role=\"listitem\"><b>Views:</b> {{question.view_count}}</mat-list-item>\n          <mat-list-item role=\"listitem\">\n            <button mat-button mat-raised-button (click)=\"openLink(question.link)\">\n              <mat-icon aria-label=\"Question link\">link</mat-icon>\n              Question link\n            </button>\n          </mat-list-item>\n        </mat-list>\n      </mat-grid-tile>\n    </mat-grid-list>\n    <mat-action-row class=\"stripe-1\">\n      <mat-chip-list *ngIf=\"question.tags\">\n        <mat-chip *ngFor=\"let tag of question.tags;\" color=\"primary\">{{tag}}</mat-chip>\n      </mat-chip-list>\n    </mat-action-row>\n  </mat-expansion-panel>\n</mat-accordion>"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: DISPLAY, HIDE, AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DISPLAY", function() { return DISPLAY; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HIDE", function() { return HIDE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _api_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./api.service */ "./src/app/api.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var DISPLAY = "show";
var HIDE = "hide";
var AppComponent = /** @class */ (function () {
    function AppComponent(apiService, snackBar) {
        this.apiService = apiService;
        this.snackBar = snackBar;
        this.questionsArray = [];
        this.tagsArray = [];
        this.tagsFormControl = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]();
        this.selectedTagsArray = [];
        this.orderArrayValues = ['desc', 'asc'];
        this.pageSize = 25;
    }
    AppComponent.prototype.ngOnInit = function () {
        this.loadPopularTags();
    };
    AppComponent.prototype.loadPopularTags = function () {
        var _this = this;
        this.loadingSpinnerClass = DISPLAY;
        this.apiService.getPopularTags().subscribe(function (data) {
            _this.tagsArray = data;
            _this.filteredTagsArray = _this.tagsFormControl.valueChanges.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["startWith"])(''), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["map"])(function (value) { return value ? _this.filterArrayByNameProperty(value, _this.tagsArray) : _this.tagsArray.slice(); }));
            _this.loadingSpinnerClass = HIDE;
        }, function (err) {
            _this.openSnackBar('Could not load tags from server! Please contact the administrator!', 'Close');
            _this.loadingSpinnerClass = HIDE;
        });
    };
    AppComponent.prototype.searchQuestions = function () {
        var _this = this;
        var fromDateTimestamp = this.getTimestampInSeconds(this.fromDate);
        var toDateTimestamp = this.getTimestampInSeconds(this.toDate);
        var tags = '';
        this.selectedTagsArray.forEach(function (tag) {
            tags += tag.name;
            tags += ';';
        });
        if (tags) {
            tags = tags.slice(0, -1);
        }
        this.loadingSpinnerClass = DISPLAY;
        this.apiService.getQuestions(fromDateTimestamp, toDateTimestamp, this.order, tags, this.title, this.pageSize, this.pageIndex).subscribe(function (data) {
            _this.questionsArray = data;
            _this.loadingSpinnerClass = HIDE;
        }, function (err) {
            _this.openSnackBar('Could not load questions from server! Please contact the administrator!', 'Close');
            _this.loadingSpinnerClass = HIDE;
        });
    };
    AppComponent.prototype.onOrderChange = function (event, order) {
        if (event.source.selected) {
            this.order = order;
        }
    };
    AppComponent.prototype.updateSelectedTagsArray = function (event, selectedTag) {
        if (event.source.selected) {
            this.selectedTagsArray.push(selectedTag);
        }
        else {
            this.selectedTagsArray = this.selectedTagsArray.filter(function (value) { return value !== selectedTag; });
        }
    };
    AppComponent.prototype.openLink = function (link) {
        window.open(link, '_blank');
    };
    AppComponent.prototype.getTimestampInSeconds = function (inputDate) {
        if (inputDate) {
            return Math.floor(inputDate.getTime() / 1000);
        }
    };
    AppComponent.prototype.filterArrayByNameProperty = function (value, array) {
        var filterValue = value.toLowerCase();
        return array.filter(function (option) {
            return option.name.toLowerCase().includes(filterValue);
        });
    };
    AppComponent.prototype.openSnackBar = function (message, action) {
        var snackBarConfig = new _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatSnackBarConfig"]();
        snackBarConfig.horizontalPosition = 'center';
        snackBarConfig.verticalPosition = 'top';
        snackBarConfig.panelClass = 'custom-snackbar';
        snackBarConfig.duration = 2000;
        this.snackBar.open(message, action, snackBarConfig);
    };
    AppComponent.prototype.nextPageAction = function (event) {
        this.pageIndex = event.pageIndex;
        this.pageSize = event.pageSize;
        this.searchQuestions();
    };
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        }),
        __metadata("design:paramtypes", [_api_service__WEBPACK_IMPORTED_MODULE_2__["ApiService"], _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatSnackBar"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _material_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./material.module */ "./src/app/material.module.ts");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_6__["AppComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _material_module__WEBPACK_IMPORTED_MODULE_4__["MaterialModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClientModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_5__["BrowserAnimationsModule"],
            ],
            providers: [],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_6__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/material.module.ts":
/*!************************************!*\
  !*** ./src/app/material.module.ts ***!
  \************************************/
/*! exports provided: MaterialModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MaterialModule", function() { return MaterialModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_material_stepper__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material/stepper */ "./node_modules/@angular/material/esm5/stepper.es5.js");
/* harmony import */ var _angular_material_list__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/list */ "./node_modules/@angular/material/esm5/list.es5.js");
/* harmony import */ var _angular_material_select__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material/select */ "./node_modules/@angular/material/esm5/select.es5.js");
/* harmony import */ var _angular_material_progress_spinner__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material/progress-spinner */ "./node_modules/@angular/material/esm5/progress-spinner.es5.js");
/* harmony import */ var _angular_material_grid_list__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material/grid-list */ "./node_modules/@angular/material/esm5/grid-list.es5.js");
/* harmony import */ var _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material/datepicker */ "./node_modules/@angular/material/esm5/datepicker.es5.js");
/* harmony import */ var _angular_material_divider__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/material/divider */ "./node_modules/@angular/material/esm5/divider.es5.js");
/* harmony import */ var _angular_material_card__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/material/card */ "./node_modules/@angular/material/esm5/card.es5.js");
/* harmony import */ var _angular_material_table__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/material/table */ "./node_modules/@angular/material/esm5/table.es5.js");
/* harmony import */ var _angular_material_paginator__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/material/paginator */ "./node_modules/@angular/material/esm5/paginator.es5.js");
/* harmony import */ var _angular_material_tabs__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/material/tabs */ "./node_modules/@angular/material/esm5/tabs.es5.js");
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/material/icon */ "./node_modules/@angular/material/esm5/icon.es5.js");
/* harmony import */ var _angular_material_autocomplete__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/material/autocomplete */ "./node_modules/@angular/material/esm5/autocomplete.es5.js");
/* harmony import */ var _angular_material_core__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @angular/material/core */ "./node_modules/@angular/material/esm5/core.es5.js");
/* harmony import */ var _angular_material_expansion__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @angular/material/expansion */ "./node_modules/@angular/material/esm5/expansion.es5.js");
/* harmony import */ var _angular_material_progress_bar__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @angular/material/progress-bar */ "./node_modules/@angular/material/esm5/progress-bar.es5.js");
/* harmony import */ var _angular_material_chips__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @angular/material/chips */ "./node_modules/@angular/material/esm5/chips.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





















var MaterialModule = /** @class */ (function () {
    function MaterialModule() {
    }
    MaterialModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_material_chips__WEBPACK_IMPORTED_MODULE_19__["MatChipsModule"], _angular_material_progress_bar__WEBPACK_IMPORTED_MODULE_18__["MatProgressBarModule"], _angular_material_expansion__WEBPACK_IMPORTED_MODULE_17__["MatExpansionModule"], _angular_material_core__WEBPACK_IMPORTED_MODULE_16__["MatRippleModule"], _angular_material_autocomplete__WEBPACK_IMPORTED_MODULE_15__["MatAutocompleteModule"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSnackBarModule"], _angular_material_icon__WEBPACK_IMPORTED_MODULE_14__["MatIconModule"], _angular_material_tabs__WEBPACK_IMPORTED_MODULE_13__["MatTabsModule"], _angular_material_table__WEBPACK_IMPORTED_MODULE_11__["MatTableModule"], _angular_material_paginator__WEBPACK_IMPORTED_MODULE_12__["MatPaginatorModule"], _angular_material_card__WEBPACK_IMPORTED_MODULE_10__["MatCardModule"], _angular_material_divider__WEBPACK_IMPORTED_MODULE_9__["MatDividerModule"], _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_8__["MatDatepickerModule"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatNativeDateModule"], _angular_material_grid_list__WEBPACK_IMPORTED_MODULE_7__["MatGridListModule"], _angular_material_progress_spinner__WEBPACK_IMPORTED_MODULE_6__["MatProgressSpinnerModule"], _angular_material_select__WEBPACK_IMPORTED_MODULE_5__["MatSelectModule"], _angular_material_list__WEBPACK_IMPORTED_MODULE_4__["MatListModule"], _angular_material_stepper__WEBPACK_IMPORTED_MODULE_3__["MatStepperModule"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatFormFieldModule"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatInputModule"], _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatButtonModule"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatToolbarModule"]],
            exports: [_angular_material_chips__WEBPACK_IMPORTED_MODULE_19__["MatChipsModule"], _angular_material_progress_bar__WEBPACK_IMPORTED_MODULE_18__["MatProgressBarModule"], _angular_material_expansion__WEBPACK_IMPORTED_MODULE_17__["MatExpansionModule"], _angular_material_core__WEBPACK_IMPORTED_MODULE_16__["MatRippleModule"], _angular_material_autocomplete__WEBPACK_IMPORTED_MODULE_15__["MatAutocompleteModule"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSnackBarModule"], _angular_material_icon__WEBPACK_IMPORTED_MODULE_14__["MatIconModule"], _angular_material_tabs__WEBPACK_IMPORTED_MODULE_13__["MatTabsModule"], _angular_material_table__WEBPACK_IMPORTED_MODULE_11__["MatTableModule"], _angular_material_paginator__WEBPACK_IMPORTED_MODULE_12__["MatPaginatorModule"], _angular_material_card__WEBPACK_IMPORTED_MODULE_10__["MatCardModule"], _angular_material_divider__WEBPACK_IMPORTED_MODULE_9__["MatDividerModule"], _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_8__["MatDatepickerModule"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatNativeDateModule"], _angular_material_grid_list__WEBPACK_IMPORTED_MODULE_7__["MatGridListModule"], _angular_material_progress_spinner__WEBPACK_IMPORTED_MODULE_6__["MatProgressSpinnerModule"], _angular_material_select__WEBPACK_IMPORTED_MODULE_5__["MatSelectModule"], _angular_material_list__WEBPACK_IMPORTED_MODULE_4__["MatListModule"], _angular_material_stepper__WEBPACK_IMPORTED_MODULE_3__["MatStepperModule"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatFormFieldModule"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatInputModule"], _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatButtonModule"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatToolbarModule"]],
        })
    ], MaterialModule);
    return MaterialModule;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! E:\Orange\stackoverflow-questions\ng-app\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map