package ro.stackoverflow.service;

import java.io.IOException;
import java.net.URI;
import java.nio.charset.Charset;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.databind.ObjectMapper;

import ro.stackoverflow.dto.ErrorResponseDto;
import ro.stackoverflow.dto.GetPopularTagsResponseDto;
import ro.stackoverflow.dto.GetQuestionsResponseDto;
import ro.stackoverflow.dto.QuestionDto;
import ro.stackoverflow.dto.TagDto;
import ro.stackoverflow.util.StackoverflowException;

@Service
public class StackoverflowServiceImpl implements StackoverflowService {

	@Value("${stackExchangeApiUrl}")
	private String stackExchangeApiUrl;

	@Override
	public List<QuestionDto> getAllQuestions(MultiValueMap<String, String> queryParams) throws StackoverflowException {

		RestTemplate template = new RestTemplate();
		template.setRequestFactory(new HttpComponentsClientHttpRequestFactory());
		template.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));

		HttpHeaders headers = new HttpHeaders();
		headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
		headers.set("Content-Type", MediaType.APPLICATION_FORM_URLENCODED_VALUE);

		String baseUrl = stackExchangeApiUrl;
		if (queryParams.get("intitle") != null) {
			baseUrl += "/search";
		} else {
			baseUrl += "/questions";
		}

		URI uri = createURI(baseUrl, queryParams);
		GetQuestionsResponseDto response;
		try {
			response = template.getForObject(uri, GetQuestionsResponseDto.class);
		} catch (HttpStatusCodeException e) {
			ObjectMapper mapper = new ObjectMapper();
			try {
				ErrorResponseDto err = mapper.readValue(e.getResponseBodyAsString(), ErrorResponseDto.class);
				throw new StackoverflowException(e.getStatusCode(), err.getError_name() + " - " + err.getError_message());
			} catch (IOException ex) {
				throw new StackoverflowException(e.getStatusCode(), ex.getMessage());
			}
		}
		return response.getItems();

	}

	@Override
	public List<TagDto> getPopularTags() throws StackoverflowException {
		RestTemplate template = new RestTemplate();
		template.setRequestFactory(new HttpComponentsClientHttpRequestFactory());

		HttpHeaders headers = new HttpHeaders();
		headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);

		UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(stackExchangeApiUrl + "/tags").queryParam("order", "desc")
				.queryParam("sort", "popular").queryParam("site", "stackoverflow");

		GetPopularTagsResponseDto response;
		try {
			response = template.getForObject(uriBuilder.toUriString(), GetPopularTagsResponseDto.class);
		} catch (HttpStatusCodeException e) {
			ObjectMapper mapper = new ObjectMapper();
			try {
				ErrorResponseDto err = mapper.readValue(e.getResponseBodyAsString(), ErrorResponseDto.class);
				throw new StackoverflowException(e.getStatusCode(), err.getError_name() + " - " + err.getError_message());
			} catch (IOException ex) {
				throw new StackoverflowException(e.getStatusCode(), ex.getMessage());
			}
		}

		return response.getItems();
	}

	/**
	 * Method used to construct the URI from a baseUrl and query params.
	 * Additionally, the <strong>sort</strong> and <strong>site</strong> params are
	 * added at the end.
	 * 
	 * @param baseUrl
	 *            the api base url
	 * @param queryParamMap
	 *            map containing the pairs of param name / param value
	 * @return The {@link URI} object
	 */
	private URI createURI(String baseUrl, MultiValueMap<String, String> queryParams) {
		UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(baseUrl);
		uriBuilder.queryParams(queryParams);
		uriBuilder.queryParam("sort", "votes");
		uriBuilder.queryParam("site", "stackoverflow");
		return uriBuilder.build(false).toUri();
	}
}
