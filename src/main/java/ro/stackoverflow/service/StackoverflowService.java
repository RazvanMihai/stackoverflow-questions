package ro.stackoverflow.service;

import java.util.List;

import org.springframework.util.MultiValueMap;

import ro.stackoverflow.dto.QuestionDto;
import ro.stackoverflow.dto.TagDto;
import ro.stackoverflow.util.StackoverflowException;

public interface StackoverflowService {

	List<QuestionDto> getAllQuestions(MultiValueMap<String, String> queryParams) throws StackoverflowException;

	List<TagDto> getPopularTags() throws StackoverflowException;
}
