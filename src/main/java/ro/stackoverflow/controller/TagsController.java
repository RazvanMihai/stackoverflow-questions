package ro.stackoverflow.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ro.stackoverflow.dto.TagDto;
import ro.stackoverflow.service.StackoverflowService;
import ro.stackoverflow.util.StackoverflowException;

@CrossOrigin
@RestController
@RequestMapping(value = "/tags")
public class TagsController {

	@Autowired
	private StackoverflowService service;

	@GetMapping
	public ResponseEntity<List<TagDto>> getPopularTags() throws StackoverflowException {
		return ResponseEntity.status(HttpStatus.OK).body(service.getPopularTags());
	}

}
