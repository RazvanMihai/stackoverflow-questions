package ro.stackoverflow.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ro.stackoverflow.dto.QuestionDto;
import ro.stackoverflow.service.StackoverflowService;
import ro.stackoverflow.util.StackoverflowException;

@CrossOrigin
@RestController
@RequestMapping(value = "/questions")
public class QuestionsController {

	@Autowired
	private StackoverflowService service;

	@GetMapping
	public ResponseEntity<List<QuestionDto>> getQuestions(@RequestParam MultiValueMap<String, String> requestParamsMap)
			throws StackoverflowException {
		return ResponseEntity.status(HttpStatus.OK).body(service.getAllQuestions(requestParamsMap));
	}

}
