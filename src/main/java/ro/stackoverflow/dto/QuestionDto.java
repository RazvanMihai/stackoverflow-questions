package ro.stackoverflow.dto;

import java.util.List;

import org.apache.commons.text.StringEscapeUtils;

public class QuestionDto {

	private String title;
	private int score;
	private List<String> tags;
	private String answer_count;
	private String view_count;
	private String link;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = StringEscapeUtils.unescapeHtml4(title);
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public List<String> getTags() {
		return tags;
	}

	public void setTags(List<String> tags) {
		this.tags = tags;
	}

	public String getAnswer_count() {
		return answer_count;
	}

	public void setAnswer_count(String answer_count) {
		this.answer_count = answer_count;
	}

	public String getView_count() {
		return view_count;
	}

	public void setView_count(String view_count) {
		this.view_count = view_count;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

}