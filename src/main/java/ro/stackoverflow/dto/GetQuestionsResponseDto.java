package ro.stackoverflow.dto;

import java.util.List;

public class GetQuestionsResponseDto {
	private List<QuestionDto> items;
	private boolean has_more;
	private int quota_max;
	private int quota_remaining;

	public List<QuestionDto> getItems() {
		return items;
	}

	public void setItems(List<QuestionDto> items) {
		this.items = items;
	}

	public boolean isHas_more() {
		return has_more;
	}

	public void setHas_more(boolean has_more) {
		this.has_more = has_more;
	}

	public int getQuota_max() {
		return quota_max;
	}

	public void setQuota_max(int quota_max) {
		this.quota_max = quota_max;
	}

	public int getQuota_remaining() {
		return quota_remaining;
	}

	public void setQuota_remaining(int quota_remaining) {
		this.quota_remaining = quota_remaining;
	}
}
