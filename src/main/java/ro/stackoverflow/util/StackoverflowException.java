package ro.stackoverflow.util;

import org.springframework.http.HttpStatus;

public class StackoverflowException extends Exception {

	private static final long serialVersionUID = -750453552415827185L;
	private HttpStatus httpStatus;

	public StackoverflowException(HttpStatus httpStatus, String message) {
		super(message);
		this.httpStatus = httpStatus;
	}

	public HttpStatus getHttpStatus() {
		return httpStatus;
	}

	public void setHttpStatus(HttpStatus httpStatus) {
		this.httpStatus = httpStatus;
	}

}
