package ro.stackoverflow.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler {

	private static final Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);

	@ExceptionHandler(StackoverflowException.class)
	public ResponseEntity<?> handleAttachmentsApiException(StackoverflowException ex) {
		logger.error("StackoverflowException occured!", ex);
		return ResponseEntity.status(ex.getHttpStatus()).body(ex.getMessage());
	}

	@ExceptionHandler(Exception.class)
	public ResponseEntity<?> handleFileFormatException(Exception ex) {
		logger.error("Exception occured!", ex);
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ex.getMessage());
	}
}
