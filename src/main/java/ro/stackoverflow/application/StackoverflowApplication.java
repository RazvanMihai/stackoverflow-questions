package ro.stackoverflow.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;

@ComponentScan("ro.stackoverflow.*")
@SpringBootApplication
public class StackoverflowApplication {
	public void addViewControllers(ViewControllerRegistry registry) {
		registry.addViewController("/stackoverflow-questions").setViewName("forward:/index.html");
	}

	public static void main(String[] args) {
		SpringApplication.run(StackoverflowApplication.class, args);

	}
}
