# Stackoverflow-questions #

The backend of the application is represented by a backend app created in **Java 8** and **Spring Boot**.
For Fontend (GUI) I created a client application using **Angular 6**, **Typescript** and **Angular Material**.

The communication between the two apps is made through HTTP Protocol. On the frontend side, I used *HTTPClient* from Angular. The entry point in the backend application is represented by The *RestControllers*, one for **tags** and the other for **questions** (I created two different controllers with two different mappings to respect the rest principles.

The structure of the backend application is the following:

1. Controller layer - used as entry point. This is where the client application is making HTTP requests.

2. Service layer - the requests recieved in the controller layer are processed here, data is prepaired to be sent to StackExchangeAPI as request. The response from the StackExchangeAPI is then processed here and returned to the Controller layer, which returns back the answer to the client app.

3. A GlobalExceptionHandling layer, used handle exceptions that occur in the backend, log them and to send a specific message to the client APP.

# Instructions to build and run #

## Requirements: Java 8, Maven 3.3+, Node, Angular CLI.

## Steps:
1. go in the *ng-app* directory and open a command line. Run `npm install`
	i. if you encounter an error symilar to this : *ERROR in node_modules/rxjs/internal/types.d.ts(81,44): error TS1005: ';' expected*, then install manually rxjs library
	ii. `npm install rxjs@6.0.0 -- save`
2. after all the libs are installed, run the build command: `ng build --base-href /stackoverflow-questions/`. This will generate compile and generate all the web files needed for the frontend application. The build automatically copies these files in the *src/resources/static* directory from the spring boot app.
3. move to the home directory (where the *pom.xml* is located) and run `mvn clean install`. This will generate the sources and the *stackoverflow-questions.jar* file in the *target* directory.
4. move to *target* directory and run `java -jar stackoverflow-questions.jar`. The app will be available at **http://localhost:8081/stackoverflow-questions/**
